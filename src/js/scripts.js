
(function () {
    var anim = $('[data-animate]');
    if (anim.length) {
        var wnd = $(window);
        var screenBlocks = [];

        anim.each(function () {
            var _this = $(this);

            wnd.scroll(function () {
                animateBlocks();
            });

            // animation for visble blocks
            var posTop = _this.offset().top,
                posScroll = wnd.scrollTop(),
                wndHeight = wnd.height();

            if (posScroll + wndHeight >= posTop) {
                screenBlocks.push(_this);
            };

            function animateBlocks() {
                var posTop = _this.offset().top,
                    posScroll = wnd.scrollTop(),
                    wndHeight = wnd.height(),
                    delayTime = 200;

                var delay = 0,
                    dataAnimate = _this.data('animate');

                if (dataAnimate != '') {
                    delay = (dataAnimate + 1) * delayTime;
                } else {
                    dataAnimate = 1;
                    delay = dataAnimate * delayTime;
                }

                setTimeout(function () {
                    if (posScroll + wndHeight >= posTop) {
                        _this.addClass('animated');

                        setTimeout(function () {
                            _this.addClass('done');
                        }, 1000);
                    };
                }, delay);
            }
        });

        setTimeout(function () {
            var count = 0;
            var intrvl = setInterval(function () {
                if (count < screenBlocks.length) {
                    screenBlocks[count].addClass('animated');
                    count++;
                } else {
                    clearInterval(intrvl);
                }
            }, 100);
        }, 400);
    }
})();