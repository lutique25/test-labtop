// Install
// ---------------------------
// If doesn't has package.json:
// npm init
// npm i --save-dev gulp gulp-load-plugins browser-sync gulp-rigger gulp-changed @babel/core @babel/preset-env gulp-babel gulp-sass gulp-sass-glob gulp-autoprefixer gulp-concat gulp-csso gulp-sourcemaps gulp-uglify gulp-rename
// ---------------------------
// If has package.json:
// npm install

// Pathes
var srcPath  = 'src/';
var distPath = 'dist/';

// Modules
var gulp        = require('gulp'),
    plugins     = require('gulp-load-plugins')(),
    browserSync = require('browser-sync').create();

// HTML
gulp.task('html', function() {
    return gulp.src(srcPath + '*.html')
        .pipe(plugins.changed(distPath))
        .pipe(plugins.rigger())
        .pipe(gulp.dest(distPath))
        .pipe(browserSync.stream());
});

// HTML-parts (rigger //-) - file connection
gulp.task('parts', function() {
    return gulp.src(srcPath + '*.html')
        .pipe(plugins.rigger())
        .pipe(gulp.dest(distPath))
        .pipe(browserSync.stream());
});

// SCSS
gulp.task('scss', function() {
     return gulp.src(srcPath + 'scss/styles.scss')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.sassGlob())
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            cascade: false
        }))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(distPath + 'css/'))
        .pipe(browserSync.stream());
});

// Javascripts
gulp.task('javascripts', function() {
    return gulp.src(srcPath + 'js/**/*.js')
        .pipe(plugins.sourcemaps.init())
        .pipe(plugins.babel({
            presets: ['@babel/env']
        }))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({suffix: '.min', prefix : ''}))
        .pipe(plugins.sourcemaps.write('.'))
        .pipe(gulp.dest(distPath + 'js/'))
        .pipe(browserSync.stream());
});

// Libraries javascript
gulp.task('libsJS', function() {
    return gulp.src([
            srcPath + 'libs/jquery-3.3.1/jquery-3.3.1.min.js',
            srcPath + 'libs/**/*.js'
        ])
        .pipe(plugins.concat('libs.min.js'))
        .pipe(plugins.uglify())
        .pipe(gulp.dest(distPath + 'js/'))
        .pipe(browserSync.stream());
});

// Libraries CSS
gulp.task('libsCSS', function() {
    return gulp.src(srcPath + 'libs/**/*.css')
        .pipe(plugins.concat('libs.min.css'))
        .pipe(plugins.csso())
        .pipe(gulp.dest(distPath + 'css/'))
        .pipe(browserSync.stream());
});

// Reload server
gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: distPath
        }
    });
});

// Watching
gulp.task('watch', function() {
    gulp.watch([srcPath + '*.html'], gulp.series('html'));
    gulp.watch([srcPath + 'parts/*.html'], gulp.series('parts'));
    gulp.watch([srcPath + 'scss/*.scss'], gulp.series('scss'));
    gulp.watch([srcPath + 'js/*.js'], gulp.series('javascripts'));
    gulp.watch([srcPath + 'libs/**/*.js'], gulp.series('libsJS'));
    gulp.watch([srcPath + 'libs/**/*.css'], gulp.series('libsCSS'));
});

// Default run
gulp.task('default', gulp.series(
        gulp.parallel('html', 'scss','javascripts', 'libsJS', 'libsCSS'),
        gulp.parallel('watch', 'server')
    ));

// Default run without server
gulp.task('no-server', gulp.series(
        gulp.parallel('html', 'scss','javascripts', 'libsJS', 'libsCSS'),
        gulp.parallel('watch')
    ));